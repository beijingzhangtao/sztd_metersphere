

export default {
  path: "/zl_script_lib",
  name: "zl_script_lib",
  redirect: "/zl_script_lib/home",
  components: {
    content: () => import('./ApiTest')
  },
  children: [
    {
      path: 'home',
      name: 'zl_script_lib_fucHome',
      component: () => import('./homepage/ApiTestHomePage'),
    },
    // {
    //   path: "project/:type",
    //   name: "fucProject",
    //   component: MsProject,
    // },
    {
      path: "report/list/:testId",
      name: "zl_script_lib_ApiReportList",
      component: () => import('./report/ApiReportList'),
    },
    {
      path: "report/view/:reportId",
      name: "zl_script_lib_ApiReportView",
      component: () => import('./report/ApiReportView'),
    },
    {
      path: "automation/report",
      name: "zl_script_lib_automation_report",
      component: () => import('./automation/report/ApiReportList'),
    },
    {
      path: "automation/report/view/:reportId",
      name: "zl_script_lib_automation_report_view",
      component: () => import('./automation/report/ApiReportView'),

    },
    {
      path: "definition/:redirectID?/:dataType?/:dataSelectRange?/:projectId?/:type?",
      name: "zl_script_lib_ApiDefinition",
      component: () => import('./definition/ApiDefinition'),
    },
    {
      path: "automation/:redirectID?/:dataType?/:dataSelectRange?/:projectId?",
      name: "zl_script_lib_ApiAutomation",
      component: () => import('./automation/ApiAutomation'),
    },
    {
      path: 'monitor/view',
      name: 'zl_script_lib_ApiMonitor',
      component: () => import('./monitor/ApiMonitor'),
    },
    {
      path: 'definition/edit/:definitionId',
      name: 'zl_script_lib_editCompleteContainer',
      component: () => import('./definition/ApiDefinition'),
    },
  ]
};
