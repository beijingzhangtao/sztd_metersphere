

export default {
  path: "/zl_rgps",
  name: "zl_rgps",
  redirect: "/zl_rgps/home",
  components: {
    content: () => import('./ApiTest')
  },
  children: [
    {
      path: 'home',
      name: 'zl_rgps_fucHome',
      component: () => import('./homepage/ApiTestHomePage'),
    },
    // {
    //   path: "project/:type",
    //   name: "fucProject",
    //   component: MsProject,
    // },
    {
      path: "report/list/:testId",
      name: "zl_rgps_ApiReportList",
      component: () => import('./report/ApiReportList'),
    },
    {
      path: "report/view/:reportId",
      name: "zl_rgps_ApiReportView",
      component: () => import('./report/ApiReportView'),
    },
    {
      path: "automation/report",
      name: "zl_rgps_automation_report",
      component: () => import('./automation/report/ApiReportList'),
    },
    {
      path: "automation/report/view/:reportId",
      name: "zl_rgps_automation_report_view",
      component: () => import('./automation/report/ApiReportView'),

    },
    {
      path: "definition/:redirectID?/:dataType?/:dataSelectRange?/:projectId?/:type?",
      name: "zl_rgps_ApiDefinition",
      component: () => import('./definition/ApiDefinition'),
    },
    {
      path: "automation/:redirectID?/:dataType?/:dataSelectRange?/:projectId?",
      name: "zl_rgps_ApiAutomation",
      component: () => import('./automation/ApiAutomation'),
    },
    {
      path: 'monitor/view',
      name: 'zl_rgps_ApiMonitor',
      component: () => import('./monitor/ApiMonitor'),
    },
    {
      path: 'definition/edit/:definitionId',
      name: 'zl_rgps_editCompleteContainer',
      component: () => import('./definition/ApiDefinition'),
    },
  ]
};
