

export default {
  path: "/zl_dycs",
  name: "zl_dycs",
  redirect: "/zl_dycs/home",
  components: {
    content: () => import('./ApiTest')
  },
  children: [
    {
      path: 'home',
      name: 'zl_dycs_fucHome',
      component: () => import('./homepage/ApiTestHomePage'),
    },
    // {
    //   path: "project/:type",
    //   name: "fucProject",
    //   component: MsProject,
    // },
    {
      path: "report/list/:testId",
      name: "zl_dycs_ApiReportList",
      component: () => import('./report/ApiReportList'),
    },
    {
      path: "report/view/:reportId",
      name: "zl_dycs_ApiReportView",
      component: () => import('./report/ApiReportView'),
    },
    {
      path: "automation/report",
      name: "zl_dycs_ApiReportList",
      component: () => import('./automation/report/ApiReportList'),
    },
    {
      path: "automation/report/view/:reportId",
      name: "zl_dycs_ApiReportView",
      component: () => import('./automation/report/ApiReportView'),

    },
    {
      path: "definition/:redirectID?/:dataType?/:dataSelectRange?/:projectId?/:type?",
      name: "zl_dycs_ApiDefinition",
      component: () => import('./definition/ApiDefinition'),
    },
    {
      path: "automation/:redirectID?/:dataType?/:dataSelectRange?/:projectId?",
      name: "zl_dycs_ApiAutomation",
      component: () => import('./automation/ApiAutomation'),
    },
    {
      path: 'monitor/view',
      name: 'zl_dycs_ApiMonitor',
      component: () => import('./monitor/ApiMonitor'),
    },
    {
      path: 'definition/edit/:definitionId',
      name: 'zl_dycs_editCompleteContainer',
      component: () => import('./definition/ApiDefinition'),
    },
  ]
};
