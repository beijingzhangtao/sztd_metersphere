

export default {
  path: "/zl_zsljc",
  name: "zl_zsljc",
  redirect: "/zl_zsljc/home",
  components: {
    content: () => import('./ApiTest')
  },
  children: [
    {
      path: 'home',
      name: 'Zl_zsljc_fucHome',
      component: () => import('./homepage/ApiTestHomePage'),
    },
    // {
    //   path: "project/:type",
    //   name: "fucProject",
    //   component: MsProject,
    // },
    {
      path: "report/list/:testId",
      name: "Zl_zsljc_ApiReportList",
      component: () => import('./report/ApiReportList'),
    },
    {
      path: "report/view/:reportId",
      name: "Zl_zsljc_ApiReportView",
      component: () => import('./report/ApiReportView'),
    },
    {
      path: "zsljc/report",
      name: "Zl_zsljcReport",
      component: () => import('./automation/report/ApiReportList'),
    },
    {
      path: "zsljc/setup",
      name: "Zl_zsljcSetup",
      component: () => import('./automation/report/Setup'),
    },
    {
      path: "automation/report/view/:reportId",
      name: "Zl_zsljc_automation_report_view",
      component: () => import('./automation/report/ApiReportView'),

    },
    {
      path: "definition/:redirectID?/:dataType?/:dataSelectRange?/:projectId?/:type?",
      name: "Zl_zsljc_ApiDefinition",
      component: () => import('./definition/ApiDefinition'),
    },
    {
      path: "automation/:redirectID?/:dataType?/:dataSelectRange?/:projectId?",
      name: "Zl_zsljc_ApiAutomation",
      component: () => import('./automation/ApiAutomation'),
    },
    {
      path: 'monitor/view',
      name: 'Zl_zsljc_ApiMonitor',
      component: () => import('./monitor/ApiMonitor'),
    },
    {
      path: 'definition/edit/:definitionId',
      name: 'Zl_zsljc_editCompleteContainer',
      component: () => import('./definition/ApiDefinition'),
    },
  ]
};
